/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "can.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
static uint8_t buffer_tx[2048] __unused = {'a','b','c','d','e','f','g','h','i','j', [10 ... 2047] = '.'};
static volatile uint32_t canrxcomplete = 0;
/* USER CODE BEGIN PV */
/* UART handler */
extern UART_HandleTypeDef huart2;
/* CAN_1 handler */
extern CAN_HandleTypeDef hcan1;
static CAN_TxHeaderTypeDef TxHeader;
static CAN_RxHeaderTypeDef RxHeader;
static uint8_t (*rxdata_p)[8] = NULL;
static uint8_t (*txdata_p)[8] = NULL;
const static uint8_t (*txstart_p)[8] = NULL;
const static uint8_t (*rxstart_p)[8] = NULL;
const static void *txend_p = NULL;
const static void *rxend_p = NULL;
static uint32_t TxMailbox;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
void SystemClock_Config(void);
static void CAN_Config(void);
static void cantxhandle(CAN_HandleTypeDef *hcan);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* Enable I-Cache---------------------------------------------------------*/
  //SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  //SCB_EnableDCache();

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_CAN1_Init();
  /* Configure the CAN peripheral */
  CAN_Config();
  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(GPIOB, LD2_Pin|LD3_Pin, GPIO_PIN_SET);
  HAL_Delay(1000);
  HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
  /* Output a message on Hyperterminal using printf function */
  printf("\n\rUART Printf Example: retarget the C library printf function to the UART\n\r");
#if 0
  HAL_UART_Transmit_DMA(&huart2,buffer_tx,2048);
  while (UART_CheckIdleState(&huart2) != HAL_OK);
  HAL_Delay(500);
  HAL_UART_Transmit_DMA(&huart2,buffer_tx,2048);
  while (UART_CheckIdleState(&huart2) != HAL_OK);
  HAL_Delay(500);
  printf("\n\rDMA transfer complete\n\r");
#else
  /* init dma buffer */
  uint8_t dmabuff[26][80] = {
		  [0 ... 24][78] = '\n',
		  [0 ... 24][79] = '\r',
		  [25][0 ... 76] = 'z',
		  [25][77] = '\n',
		  [25][78] = '\r',
		  [25][79] = '\0'
  };
  uint8_t (*dmabuff_p)[80] = dmabuff;
  for (char i = 'a'; i <= 'y'; i++, dmabuff_p++) {
	  memset(dmabuff_p, i, 78);
  }
  /* init message separator buffer */
  uint8_t termbuff[80] = {
		  [0 ... 76] = '-',
		  [77] = '\n',
		  [78] = '\r',
		  [79] = '\0'
  };
  /* dma mem to uart */
  if (HAL_UART_Transmit_DMA(&huart2, &termbuff[0], strlen((char *)termbuff)) != HAL_OK)
  {
    Error_Handler();
  }
  while (UART_CheckIdleState(&huart2) != HAL_OK);
  HAL_Delay(200);
  if (HAL_UART_Transmit_DMA(&huart2, &dmabuff[0][0], strlen((char *)dmabuff)) != HAL_OK)
  {
    Error_Handler();
  }
  while (UART_CheckIdleState(&huart2) != HAL_OK);
  HAL_Delay(200);
  printf("%s", termbuff);
#endif
  printf("** Test finished successfully. **\n\r");
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  /* init can variables */
  uint8_t rxbuff[26][80] = {[0 ... 25][0 ... 79] = '\0'};
  txstart_p = (uint8_t (*)[8])&dmabuff[0][0];
  rxstart_p = (uint8_t (*)[8])&rxbuff[0][0];
  txend_p = &dmabuff[25][79];
  rxend_p = &rxbuff[25][79];
  txdata_p = (uint8_t (*)[8])txstart_p;
  rxdata_p = (uint8_t (*)[8])txstart_p;
  while (1)
  {
    /* USER CODE END WHILE */
    if (canrxcomplete == 1) {
    	if (HAL_UART_Transmit_DMA(&huart2, &rxbuff[0][0], strlen((char *)rxbuff)) != HAL_OK)
    	{
    		Error_Handler();
    	}
    	while (UART_CheckIdleState(&huart2) != HAL_OK);
    	canrxcomplete = 0;
    }
    HAL_Delay(500);
    /* start can send message */
    cantxhandle(&hcan1);
    HAL_Delay(500);
    printf("can state: %x err: %ld\n\r", HAL_CAN_GetState(&hcan1), HAL_CAN_GetError(&hcan1));
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 128;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART3 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}

/**
  * @brief  Configures the CAN.
  * @param  None
  * @retval None
  */
static void CAN_Config(void)
{
	CAN_FilterTypeDef sFilterConfig;

	/*##-2- Configure the CAN Filter ###########################################*/
	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;

	if (HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK)
	{
		/* Filter configuration Error */
		Error_Handler();
	}

	/*##-3- Start the CAN peripheral ###########################################*/
	if (HAL_CAN_Start(&hcan1) != HAL_OK)
	{
		/* Start Error */
		Error_Handler();
	}

	/*##-4- Activate CAN TX RX notification #######################################*/
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK)
	{
		/* Notification Error */
		Error_Handler();
	}
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
	{
		/* Notification Error */
		Error_Handler();
	}
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_FULL) != HAL_OK)
	{
		/* Notification Error */
		Error_Handler();
	}

	/*##-5- Configure Transmission process #####################################*/
	TxHeader.StdId = 0x321;
	TxHeader.ExtId = 0U;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.TransmitGlobalTime = DISABLE;
}

/* can tx interrupts handler */
static void cantxhandle(CAN_HandleTypeDef *hcan)
{
	uint32_t buffdiff = 0;

	if (txdata_p == NULL || canrxcomplete == 1) {
		return;
	}

	/* Reset the transmit data pointer */
	if ((void *)txend_p < (void *)txdata_p) {
		txdata_p = (uint8_t (*)[8])txstart_p;
	}

	TxHeader.DLC = 8;
	buffdiff = (void *)txend_p - (void *)txdata_p;
	if (buffdiff < 8) {
		TxHeader.DLC = buffdiff;
	}

	/* Send TX message */
	if (HAL_CAN_AddTxMessage(hcan, &TxHeader, (uint8_t *)txdata_p, &TxMailbox) != HAL_OK)
	{
		/* Transmission request Error */
		Error_Handler();
	}

	txdata_p++;
}

/* can rx interrupts handler */
void canrxhandle(CAN_HandleTypeDef *hcan)
{
	if (rxdata_p == NULL || canrxcomplete == 1) {
		return;
	}

	/* Reset the receive data pointer */
	if ((void *)rxend_p < (void *)rxdata_p) {
		rxdata_p = (uint8_t (*)[8])rxstart_p;
		/* do dma to uart in the main() */
		canrxcomplete = 1;
	}

	/* Get RX message */
	if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, (uint8_t *)rxdata_p) != HAL_OK)
	{
		/* Reception Error */
		Error_Handler();
	}

	rxdata_p++;
}

/**
  * @brief  CAN defined callbacks
  * @param  hcan: pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @retval None
  */
void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan)
{
	cantxhandle(hcan);
}
void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan)
{
	cantxhandle(hcan);
}
void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan)
{
	cantxhandle(hcan);
}
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	canrxhandle(hcan);
}
void HAL_CAN_RxFifo0FullCallback(CAN_HandleTypeDef *hcan)
{
	canrxhandle(hcan);
}
void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan)
{
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  static unsigned char flip = 0;
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
	HAL_GPIO_WritePin(GPIOB, LD3_Pin, flip ? GPIO_PIN_SET : GPIO_PIN_RESET);
	flip = ~flip;
	HAL_Delay(500);
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
